[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687579.svg)](https://doi.org/10.5281/zenodo.4687579)

# WIND ENERGY POTENTIAL


## Repository structure

```
nuts3_dataset\datapackage.json		-- Datapackage JSON file with themain meta-data
nuts3_dataset\output_potential.csv	-- CSV data with the NUTS3 data set
raster						-- folder with raster data
```

## Description of the task

The present task provides data with following characteristics:

<table>
  <tr>
    <td> </td>
    <td>Spatial resolution</td>
    <td>Temporal resolution</td>
  </tr>
  <tr>
    <td>Wind potential</td>
    <td>EU28</td>
    <td>yearly</td>
  </tr>
</table>

**Table 1.** Characteristics of data provided within Task 2.6 Renewable energy sources data collection and potential review.

In this task, we collected and re-elaborated data on energy potential of renewable sources at national level, in order to build datasets for all EU28 countries at NUTS3 level. We considered the following renewable sources: biomass, waste and wastewater, shallow geothermal, wind, and solar energy. These data will be used in the toolbox to map the sources of renewable thermal energy across the EU28 and support energy planning and policy.

### Wind power
Data show the total energy potential of wind in the EU28 at NUTS3 level. The original dataset is the Wind Global Atlas from IRENA and developed by the DTU (Kogens Lyngby, Denmark). Raster data with the power density of wind at 50, 100 and 200 m have been aggregated at NUTS3 level in Grass GIS, through the Corine Land Cover and by excluding urban areas, bird connectivity corridors, mountain peaks over 2500m and protected areas from the Natura 2000 framework.

Data on the wind-energy potential in W/m2 have been drawn by the Global Wind Atlas (DTU Department of Wind Energy) for 50, 100, 200 m hub heights.

#### Methodology

For the calculation of wind energy potential we considered only areas with low or sparse vegetation, and bare and burnt areas (classes from 3.2.1. to 3.2.4. and 3.3.3., 3.3.4. of Corine Land Cover - CLC). We then excluded the following areas according to sustainability criteria:

* Areas above 2500 m.a.s.l.;
* A 1 km buffer from urban areas (classes from 1.1.1. to 1.4.2. of the CLC);
* Corridors for bird connectivity (Common Database on Designated Areas [148]);
* Exclusion of protected areas of the Nature 2000 network [149].

We then considered a distance among wind hubs of 300 m and found the most frequent value (median) of potential from wind energy for each NUTS3. The results are stored in a .csv table and a raster layer in the repository but they can also be visualized as a color-coded map created in QGIS, as shown in Figure 52.

## Limitations of data

The data here calculated are estimations of the energy potential from renewable energy sources. The hypotheses we made when deciding what data to consider, when re-elaborating the data at more aggregated territorial levels and finally when deciding how to convey the results, can influence the results.

In some cases, we underestimated the actual potential (biomass), by downscaling the available resource for sustainability reasons, in others, we overestimated the potential (wind, solar) due to our assumption of using all available areas, according only to some GIS sustainable criteria, where energy generation is feasible without considering economic profitability.

The potentials here reported **do not account for any type of energy conversion**: when estimating the actual potential, the user will need to choose the technology through which the potential can be exploited (for example COP for the wastewater treatment plant or the efficiency for solar thermal, photovoltaic and wind).

For these reasons, the data must be considered **as indicators, rather than absolute figures** representing the actual energy potential of renewable sources in a territory.

## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Müller (e‐think), Michael Hartner (TUW), Tobias Fleiter, Anna‐Lena Klingler, Matthias Kühnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW) Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 www.hotmaps-project.eu

## Authors

Giulia Garegnani<sup>*</sup>, 
Chiara Scaramuzzino<sup>*</sup>

<sup>*</sup> Eurac Research 

Institute for Renewable Energy
VoltaStraße/Via Via A. Volta 13/A
39100 Bozen/Bolzano

## License

Copyright © 2016-2018: Giulia Garegnani <giulia.garegnani@eurac.edu>,  Pietro Zambelli <pietro.zambelli@eurac.edu>
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement

We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.